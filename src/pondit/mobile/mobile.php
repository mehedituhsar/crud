<?php

namespace App\pondit\mobile;
use PDO;
class mobile
{
    public $id='';
    public $title='';
    public $conn='';
    public $dbuser='root';
    public $dbpass='';

    public function __construct()
    {
        $this->conn=new PDO("mysql:host=localhost;dbname=pondit",$this->dbuser,$this->dbpass);
    }

    public function setData($data="")
    {

      if (array_key_exists('mobile_model', $data) and !empty($data)) {
           $this->title = $data['mobile_model'];
        }
        return $this;
  }

    public function store(){
        $query="INSERT INTO 'mobile_names' ('id', 'title') VALUES (:id, :title)";
        $stmt=$this->conn->prepare( $query);
        $result=$stmt->execute(array(
            ':id'=>null,
            ':title'=>$this->title
        ));
        echo $result;


    }

    public function index(){

    }
    public function create(){

    }
    public function delete(){

    }
    public function update(){

    }

}